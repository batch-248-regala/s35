const express = require("express");
const mongoose = require("mongoose");

const app = express();

const port = 4000;

const users = [];

//MongoDB Connection
mongoose.connect("mongodb+srv://admin:admin123@clusterbatch248.w2uuxss.mongodb.net/s35?retryWrites=true&w=majority", {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

//connection to database
let db = mongoose.connection;

//error handling in connecting
db.on("error", console.error.bind(console, "connection error"));
//this will be triggered if the connection is successful
db.once("open", () => console.log("We're connected to MongoDB Atlas!"));

//Mongoose Schema
const taskSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Task name is required!"]
  },
  status: {
    type: String,
    default: "pending"
  }
});

//Models
const Task = mongoose.model("Task", taskSchema);

//Setup for allowing the server to handle data from requests
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//Creating a new task
app.post("/tasks", (req, res) => {
  /*
    Check if there are duplicate tasks
    "findOne" is a Mongoose method that acts similar to "find" of MongoDB
    findOne() returns the first document that matches the search criteria
    If there are no matches, the value of result is null
  */
  Task.findOne({ name: req.body.name }, (err, result) => {
    if (result != null && result.name == req.body.name) {
      // If a document was found and the document's name matches the information sent via the client/Postman
      // Return a message to the client/Postman
      return res.send(`Duplicate task found!`);
    } else {
      // If no document was found, create a new task and save it to the database
      let newTask = new Task({
        name: req.body.name
      });
      // The "save" method will store the information to the database
      newTask.save((saveErr, savedTask) => {
        if (saveErr) {
          // If there are errors in saving
          // Will print any errors found in the console
          console.error(saveErr);
          return res.status(500).send("Could not save task");
        } else {
          // No error found while creating the document
          // Return a status code of 201 for created
          // Sends a message "New task created" on successful creation
          console.log("Task is saved!");
          return res.status(201).send("New task created");
        }
      });
    }
  });
});

//Retrieve all tasks from the database
app.get("/tasks", (req, res) => {
  Task.find({}, (error, results) => {
    if (error) {
      console.error(error);
      return res.status(500).send("Could not retrieve tasks");
    } else {
      // Return a JSON object containing all the documents found
      return res.status(200).json(results);
    }
  });
});

//Activity

  // Registering a user
  // Business Logic
  /*
  1. Add a functionality to check if there are duplicate tasks
    - If the user already exists in the database, we return an error
    - If the user doesn't exist in the database, we add it in the database
  2. The user data will be coming from the request's body
  3. Create a new User object with a "username" and "password" fields/properties
  */

// Registering a user
app.post('/signup', (req, res) => {
  const username = req.body.username;
  const password = req.body.password;

  // Business Logic
  // Check for empty username or password
  if (!username || !password) {
    return res.status(400).send("Username and password must be provided");
  }

  // Check for duplicate users
  const userExists = users.some(user => user.username === username);
  if (userExists) {
    return res.status(409).send("Duplicate username found");
  }

  // Create a new user object
  const newUser = { username, password };
  users.push(newUser);

  return res.status(201).send("New user registered");
});

app.listen(port, () => console.log(`App is listening at http://localhost:${port}`));
